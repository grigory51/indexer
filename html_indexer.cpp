/*
 * html_indexer.cpp
 *
 *  Created on: Feb 25, 2014
 *      Author: grigory51
 */

#include "html_indexer.h"

using std::string;
using std::vector;
using std::cout;
using std::endl;

using htmlcxx::HTML::ParserDom;
using htmlcxx::HTML::Node;

HTMLIndexer::HTMLIndexer(string input) {
	ParserDom parser;
	tree<Node> dom = parser.parseTree(input);

	int docId = 0;
	tree<Node>::iterator it, end = dom.end();

	for (it = dom.begin(); it != end; it++) {

		if (it->tagName() == "p") {
			string doc = get_child_content(dom, it);
			docId++;
			this->AddSnippet(doc, docId);
			this->AddString(doc, docId);
		}
	}
}

void HTMLIndexer::AddSnippet(string snippet, int docId) {
	this->snippets[docId] = snippet;
}

string HTMLIndexer::GetSnippet(int docId) {
	return this->snippets[docId];
}

string get_child_content(tree<Node> const & dom,
		tree<Node>::iterator const & parent) {
	string result = "";
	try {
		for (unsigned i = 0; i < dom.number_of_children(parent); i++) {
			tree<Node>::iterator it = dom.child(parent, i);
			if (!it->isTag() && !it->isComment()) {
				result += it->text();
			}
		}
	} catch (std::exception & e) {
	}

	return result;
}

