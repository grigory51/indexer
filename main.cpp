/*
 *  main.cpp
 *  Created on: Feb 24, 2014
 *  Author: grigory51
 */
#include <iostream>
#include <fstream>
#include <vector>
#include "html_indexer.h"

using std::cout;
using std::ifstream;
using std::endl;
using std::string;
using std::vector;

int main(int argc, char *argv[]) {

	if (argc > 1) {
		string file_name = argv[1];
		ifstream file;
		file.open(file_name);

		if (file) {
			string document;

			file.seekg(0, std::ios::end);
			document.resize(file.tellg());
			file.seekg(0, std::ios::beg);
			file.read(&document[0], document.size());
			file.close();

			HTMLIndexer indexer(document);

			if (argc > 2) {
				if (strcmp(argv[2], "--stat") == 0) {
					cout << "Info: " << file_name << endl;
					cout << "Avg token len: " << indexer.GetAvgTokenLength()
							<< endl;
					cout << "Avg token count: " << indexer.GetTokenCount()
							<< endl;
				} else if (strcmp(argv[2], "--search") == 0) {
					int n = argc - 3;
					string* words = new string[n];
					for (int i = 0; i < n; i++) {
						words[i] = argv[i+3];
					}
					vector<int> result = indexer.Search(words, n);
					for(unsigned int i=0; i < result.size(); i++) {
						cout << result[i] << ": " << indexer.GetSnippet(result[i]) <<endl;
					}
				}
			} else {
				cout << indexer.IndexToCSV();
			}
		}
	}
	return 0;
}

