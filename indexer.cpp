#include <iostream>
#include "indexer.h"

using std::string;
using std::wstring;
using std::tr1::unordered_map;
using std::vector;
using std::less;
using std::lower_bound;


Indexer::Indexer(string s[]) {
	int size = sizeof(s) / sizeof(*s);
	for (int i = 0; i < size; i++) {
		this->AddToken(s[i], i);
	}
}

Indexer::Indexer() {

}

void Indexer::AddToken(string item, int docId) {
	vector<int>* i = &this->index[item];
	this->total_token_len += item.length() / 2;

	vector<int>::iterator it = lower_bound(i->begin(), i->end(), docId,
			less<int>());
	i->insert(it, docId);

}

void Indexer::AddString(string item, int docId) {
	std::wstring wsTmp(item.begin(), item.end());

	vector<string> tokens = this->Tokennise(wsTmp);
	for (vector<string>::iterator it = tokens.begin(); it != tokens.end();
			it++) {
		this->AddToken(*it, docId);
	}
}

float Indexer::GetAvgTokenLength() {
	if (this->index.size() > 0) {
		return ((float) this->total_token_len / (float) this->index.size());
	} else {
		return 0;
	}
}

int Indexer::GetTokenCount(void) {
	return this->index.size();
}

vector<int> Indexer::Search(const std::string words[], int words_count) {
	vector<int> result;
	vector<vector<int>*> postingLists;
	if (words_count > 0) {
		bool is_skipped_words = false;
		for (int i = 0; i < words_count; i++) {

			unordered_map<string, vector<int>>::iterator temp_posting_list_map_element =
					(&this->index)->find(words[i]);

			if (temp_posting_list_map_element != (&this->index)->end()) {
				vector<int>* temp_posting_list =
						&temp_posting_list_map_element->second;

				//вставка в вектор по длине листа для более быстрого пересечения
				vector<vector<int>*>::iterator it = lower_bound(
						postingLists.begin(), postingLists.end(),
						temp_posting_list, vector_size_less<vector<int>*>());

				postingLists.insert(it, temp_posting_list);
			} else {
				is_skipped_words = true;
				break;
			}
		}
		if (!is_skipped_words) {
			result = *postingLists[0];
			for (unsigned int i = 1; i < postingLists.size(); i++) {
				vector<int> temp = result;
				vector<int>::iterator it = std::set_intersection(
						postingLists[i]->begin(), postingLists[i]->end(),
						temp.begin(), temp.end(), result.begin());

				result.resize(it - result.begin());
				if (result.size() == 0) {
					break;
				}
			}

		}
	}
	return result;
}

string Indexer::IndexToCSV() {
	string result;

	for (auto indexIterator = this->index.begin();
			indexIterator != this->index.end(); indexIterator++) {
		vector<int>::iterator postingListItterator;

		result += indexIterator->first;
		for (postingListItterator = indexIterator->second.begin();
				postingListItterator != indexIterator->second.end();
				postingListItterator++) {

			std::stringstream sstm;
			sstm << "," << *postingListItterator;
			result += sstm.str();
		}
		result += '\n';
	}
	return result;
}

vector<string> Indexer::Tokennise(const wstring str) {
	vector<wstring> wresult;
	vector<string> result;

	wchar_t delimeters[] = L"!@#$%^&*()_+!\"№;:?-'\\/<>,`~ .[]{}\t\n\r\b\f\v";
	wchar_t * doc = const_cast<wchar_t*>(str.c_str());
	wchar_t * pch;
	wchar_t * context = NULL;
	setlocale(LC_ALL, "");
	pch = wcstok(doc, delimeters, &context);
	while (pch != NULL) {
		chrtolower(pch);
		wresult.push_back(pch);
		pch = wcstok(NULL, delimeters, &context);
	}

	for (vector<wstring>::iterator it = wresult.begin(); it != wresult.end();
			it++) {
		string temp(it->begin(), it->end());
		result.push_back(temp);
	}

	return result;
}

//Functions
void chrtolower(wchar_t * str) {
	for (unsigned int i = 0; str[i]; i++) {
		*(str + i) = towlower(*(str + i));
	}
}

std::wstring string_to_wstring(const std::string& in, std::locale loc =
		std::locale()) {
	std::wstring out;
	std::string::const_iterator i(in.begin()), ie(in.end());

	for (; i != ie; ++i) {
		out += std::use_facet<std::ctype<wchar_t> >(loc).widen(*i);
	}

	return out;
}

