################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CC_SRCS += \
../vendor/htmlcxx/CharsetConverter.cc \
../vendor/htmlcxx/Extensions.cc \
../vendor/htmlcxx/Node.cc \
../vendor/htmlcxx/ParserDom.cc \
../vendor/htmlcxx/ParserSax.cc \
../vendor/htmlcxx/Uri.cc \
../vendor/htmlcxx/utils.cc 

OBJS += \
./vendor/htmlcxx/CharsetConverter.o \
./vendor/htmlcxx/Extensions.o \
./vendor/htmlcxx/Node.o \
./vendor/htmlcxx/ParserDom.o \
./vendor/htmlcxx/ParserSax.o \
./vendor/htmlcxx/Uri.o \
./vendor/htmlcxx/utils.o 

CC_DEPS += \
./vendor/htmlcxx/CharsetConverter.d \
./vendor/htmlcxx/Extensions.d \
./vendor/htmlcxx/Node.d \
./vendor/htmlcxx/ParserDom.d \
./vendor/htmlcxx/ParserSax.d \
./vendor/htmlcxx/Uri.d \
./vendor/htmlcxx/utils.d 


# Each subdirectory must supply rules for building sources it contributes
vendor/htmlcxx/%.o: ../vendor/htmlcxx/%.cc
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -O0 -g3 -Wall -c -std=c++11 -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


