/*
 *  indexer.h
 *
 *  Created on: Feb 24, 2014
 *  Author: grigory51
 */
#ifndef _INDEXER
#define _INDEXER

#include <vector>
#include <string>
#include <sstream>
#include <algorithm>
#include <tr1/unordered_map>

class Indexer {
	private:
		std::tr1::unordered_map<std::string, std::vector<int> > index;
		int total_token_len = 0;
	private:
		std::vector<std::string> Tokennise(const std::wstring);

	public:
		Indexer(std::string[]);
		Indexer();

		void AddToken(std::string, int);
		void AddString(std::string, int);
		float GetAvgTokenLength(void);
		std::vector<int> Search(const std::string*, int);
		int GetTokenCount(void);
		std::string IndexToCSV(void);
};

void chrtolower(wchar_t *);
std::wstring string_to_wstring(const std::string&, std::locale loc);


template<typename T>
struct vector_size_less: public std::binary_function<T, T, bool> {
	bool operator()(const T  x, const T  y) const {
		return x->size() < y->size();
	}
};

#endif

