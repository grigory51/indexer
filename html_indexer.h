/*
 * html_indexer.h
 *
 *  Created on: Feb 25, 2014
 *      Author: grigory51
 */

#ifndef HTML_INDEXER_H_
#define HTML_INDEXER_H_

#include <string>
#include <iostream>
#include <tr1/unordered_map>

#include "indexer.h"
#include "vendor/htmlcxx/ParserDom.h"

class HTMLIndexer: public Indexer {
private:
	std::tr1::unordered_map<int, std::string> snippets;
	void AddSnippet(std::string, int);
public:
	HTMLIndexer(std::string);
	std::string GetSnippet(int);
};

std::string get_child_content(tree<htmlcxx::HTML::Node> const &,
		tree<htmlcxx::HTML::Node>::iterator const &);

#endif
